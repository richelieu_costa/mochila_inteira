/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ladrao_inteligente;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import ladrao_inteligente.ClassValue;

/**
 *
 * @author  Kevin Veloso 11318626 e Richelieu Costa 11328634
 */
public class Ladrao_inteligente {

    /**
     * @param args the command line arguments
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        
    
        
        ArrayList<Integer> pesos = new ArrayList<Integer>();
        ArrayList<Integer> lucros = new ArrayList<Integer>();
        
        
        int capacidade_mochila;
        int qtd_itens;
        
//      String fileDir=new String("mochila01.txt.txt");
//      DataInputStream dataIn = new DataInputStream(new FileInputStream(fileDir));
//      dataIn.readChar();
//          
//       capacidade_mochila=dataIn.readInt();
//         qtd_itens=dataIn.readInt();
//      
//          for(int i=0;i<qtd_itens-1;i++){
//              System.out.print(" aqui\t"+i);
//              dataIn.readChar();
//              pesos.add(dataIn.readInt());
//          
//          
//          
//        }
//          
//      for(int i=0;i<qtd_itens-1;i++){
// 
//          dataIn.readChar();
//          
//              lucros.add(dataIn.readInt());
//        
//          
//        }
        
        capacidade_mochila=6;
        qtd_itens=4;
        
        pesos.add(3);
        pesos.add(2);
        pesos.add(4);
        pesos.add(3);
        
        lucros.add(3);
        lucros.add(4);
        lucros.add(5);
        lucros.add(3);
        
 
        System.out.println(pesos);
        System.out.println(lucros);
        
        HashMap<String, ClassValue> tabela = new HashMap<String, ClassValue>();
        
        int index_produto = pesos.size() - 1;
        int capacidade = 0;
        
        while(capacidade <= capacidade_mochila) {
            
            if((capacidade - pesos.get(index_produto)) < 0) {
                
                ClassValue valor = new ClassValue(0, false);
                tabela.put(capacidade+","+(index_produto + 1), valor);
                
            }else {
                
                ClassValue valor = new ClassValue(lucros.get(index_produto), true);
                tabela.put((capacidade+","+(index_produto + 1)), valor);
                
            }
            System.out.println(" PRODUTO "+(index_produto + 1)+": "+(capacidade+","+(index_produto + 1)) +" -> "+ tabela.get((capacidade+","+(index_produto + 1))).getLucro());
            
            capacidade++; 
        }
                
        index_produto--;
        
        
        while(index_produto >= 0) {
                   
            capacidade = 0;
        
            while(capacidade <= capacidade_mochila) {
                
                if((capacidade - pesos.get(index_produto)) < 0) {
                    
                    ClassValue valor = new ClassValue(tabela.get(capacidade+","+(index_produto+2)).getLucro(), false);
                    tabela.put((capacidade+","+(index_produto+1)), valor);
                    
                } else {
                    
                    ClassValue max = max(tabela.get(capacidade+","+(index_produto+2)).getLucro(), tabela.get((capacidade-pesos.get(index_produto))+","+ (index_produto+2)).getLucro() + lucros.get(index_produto));
                    tabela.put((capacidade+","+(index_produto+1)), max);  
                    
                }
                
                System.out.println(" PRODUTO "+(index_produto + 1)+": "+(capacidade+","+(index_produto + 1)) +" -> "+ tabela.get((capacidade+","+(index_produto + 1))).getLucro());
                  
                capacidade++;
            }
            
           index_produto--;
        }
        
                
        capacidade = capacidade_mochila;
        
        boolean[] produtos_roubados = new boolean[pesos.size()];
                
        ClassValue instancia = null;
    
        for(int index_prod = 0; index_prod < pesos.size(); index_prod++) {
                        
            instancia = tabela.get((capacidade+","+(index_prod+1)));

                        
            if(instancia.getPego()) {
                
                produtos_roubados[index_prod] = true;
                
                capacidade = capacidade - pesos.get(index_prod);
                
                
            } else {
                
                produtos_roubados[index_prod] = false;
                
            }
                        
        }
        
        for(int i=0; i < produtos_roubados.length; i++) {
            System.out.println("Produto "+(i+1)+": "+produtos_roubados[i]);
        }
        
        
    }
    
    public static ClassValue max(int valor1, int valor2){
        int lucro;
        boolean x;

        if( valor1 > valor2) {

            x = false;
            lucro = valor1;


        } else {

            x = true;
            lucro = valor2;


        }


        return new ClassValue(lucro, x);
    }
}
   